# Node.js Application Readme

## Steps to Setup:

1. Clone the repository:

   ```bash
   git clone <repository_url>
2. Install Docker and Node.js dependencies:
    * Docker
    * Run npm install:

        ```bash
        npm install
3. Build and run Docker containers:

    ```bash
    docker-compose build
    docker-compose up -d
4. Running the application:
    * Start the application:

        ```bash
        npm run start
    * Run unit tests:
    
        ```bash
        npm run test
5. API Endpoints:
    * Register a user:
        * Method: POST
        * Route: {host}/api/register
        * Body:

            ```json
            {
                "username": "",
                "password": ""
            }
    * User login:
        * Method: POST
        * Route: {host}/api/login
        * Body:

            ```json
            {
                "username": "",
                "password": ""
            }
    * Get user information:
        * Method: GET
        * Route: {host}/api/user
        * Headers: Authorization with bearer token
    * Create a new user:
        * Method: POST
        * Route: {host}/api/user
        * Headers: Authorization with bearer token
        * Body:

            ```json
            {
                "userName": "",
                "accountNumber": "",
                "emailAddress": "",
                "identityNumber": 0
            }
    * Get user by ID:
        * Method: GET
        * Route: {host}/api/user/{id}
        * Headers: Authorization with bearer token
    * Update user information:
        * Method: PUT
        * Route: {host}/api/user/{id}
        * Headers: Authorization with bearer token
        * Body:

            ```json
            {
                "userName": "",
                "accountNumber": "",
                "emailAddress": "",
                "identityNumber": 0
            }
    * Delete user:
        * Method: DELETE
        * Route: {host}/api/user/{id}
        * Headers: Authorization with bearer token