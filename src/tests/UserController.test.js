const request = require('supertest');
const app = require('../index');

let token,id; // Global variable to store the token for subsequent tests

function getRandomDecimal(min, max) {
  min = Math.ceil(min); // Ensure minimum is an integer
  max = Math.floor(max); // Ensure maximum is an integer
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

describe('Auth Controller', () => {
  // Registration test
  const username = `${getRandomDecimal(1000000, 9999999)}`;
  const password = 'testing1234';
  it('should create a new user on registration', async () => {
    const { expect } = await import('chai');
    const res = await request(app)
      .post('/api/register')
      .send({ username, password });
    expect(res.statusCode).to.equal(201); // Expect created status (TDD style)
    // Use toEqual or to.be.equal for value comparison
    expect(res.body.data).to.have.property('_id'); // Expect user ID (TDD style)
  });

  // Login test (using previously registered user)
  it('should login a registered user and provide a token', async () => {
    const { expect } = await import('chai');
    const res = await request(app)
      .post('/api/login')
      .send({ username, password });

    if (res.body.token) {
      token = res.body.token;
    }
    // Store token for future tests

    expect(res.statusCode).to.equal(200); // Expect successful login status (TDD style)
    expect(res.body).to.have.property('token'); // Expect token in response (TDD style)
  });
});

// User Controller tests (if applicable) - Assuming a separate route for users
describe('User Controller', () => {
  it('should create a new user with valid Bearer token', async () => {
    const { expect } = await import('chai');
    const userData = {
      userName: `${getRandomDecimal(1000000, 9999999)}`,
      accountNumber: `${getRandomDecimal(1000000, 9999999)}`,
      emailAddress: `${getRandomDecimal(1000000, 9999999)}@example.com`,
      identityNumber: getRandomDecimal(1000000, 9999999)
    };
    const res = await request(app)
      .post('/api/user')
      .set('Authorization', `Bearer ${token}`) // Set Authorization header with token
      .send(userData);
    if(res.body.data){
      id = res.body.data._id;
    }
    expect(res.statusCode).to.equal(201); // Expect created status (TDD style)
    expect(res.body.data).to.have.property('_id'); // Expect user ID (TDD style)
  });

  it('should update user with valid Bearer token', async () => {
    const { expect } = await import('chai');
    const userData = {
      userName: `${getRandomDecimal(1000000, 9999999)}`,
      accountNumber: `${getRandomDecimal(1000000, 9999999)}`,
      emailAddress: `${getRandomDecimal(1000000, 9999999)}@example.com`, // Ensure valid email format
      identityNumber: getRandomDecimal(1000000, 9999999)
    };
    const res = await request(app)
      .put(`/api/user/${id}`)
      .set('Authorization', `Bearer ${token}`) // Set Authorization header with token
      .send(userData);
    expect(res.statusCode).to.equal(200); // Expect created status (TDD style)
    expect(res.body.data).to.have.property('_id'); // Expect user ID (TDD style)
  });
  // Add more tests for other user CRUD operations (if applicable)
});
