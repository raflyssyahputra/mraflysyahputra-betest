const express = require('express');
const mongoose = require('mongoose');

const routes = require('./routes/index.js');
const connectDB = require('./connection/db.js'); // Import the database connection function

const cors = require('cors');
require('dotenv').config();

// ... configure your Express app ...

// const redis = require('redis');

// let redisClient;
// let redisURL = process.env.REDIS_URL
// if (redisURL) {
//   // create the Redis client object
//   redisClient = redis.createClient({
//     host: process.env.REDIS_HOST,
//     port: process.env.REDIS_PORT,
//     password: process.env.REDIS_PASSWORD
//   });

//   try {
//     // connect to the Redis server
//     redisClient.connect();
//     console.log(`Connected to Redis successfully!`);
//   } catch (e) {
//     console.error(`Connection to Redis failed with error:`);
//     console.error(e);
//   }
// }

const app = express();
const port = process.env.PORT || 3000;
app.use(cors());

// Call the database connection function
connectDB(process.env.MONGO_USERNAME, process.env.MONGO_PASSWORD, process.env.MONGO_DBNAME); // Connect to MongoDB before starting the server

app.use(express.json());
app.get('/', (req, res) => {
  res.send('Hello, World!');
});
app.use('/api', routes);

// Start the server
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

module.exports = app;