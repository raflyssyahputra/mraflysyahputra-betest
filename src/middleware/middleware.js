const jwt = require('jsonwebtoken');
const redis = require('redis');
const Auth = require('../models/Auth');
require('dotenv').config();

const redisClient = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASSWORD
});
redisClient.connect();

async function verifyToken(req, res, next) {
  const authHeader = req.headers.authorization;

  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }

  const token = authHeader.split(' ')[1];

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const authUsername = decoded.authUsername;

    // Check Redis cache for user data
    const cachedAuth = await redisClient.get(`redis_mraflysyahputra_ betest-authLogin:${authUsername}`);
    let authCache;

    if (cachedAuth) {
        authCache = JSON.parse(cachedAuth);
    } else {
        // Fetch user from MongoDB if not in cache
        authCache = await Auth.findById(authUsername);
        if (!authCache) {
            return res.status(401).json({ message: 'Invalid token' });
        }
        // Update Redis cache with user data
        await redisClient.set(`redis_mraflysyahputra_ betest-authLogin:${authUsername}`, JSON.stringify(user), 'EX', 3600); // Cache for 1 hour
    }

    req.user = authCache;
    next(); // Allow request to proceed with the user object
  } catch (err) {
    console.error('Error verifying token:', err);
    return res.status(401).json({ message: 'Invalid token' });
  }
}

module.exports = verifyToken;
