const mongoose = require('mongoose');

const connectDB = async (username, password, dbName) => {
  try {
    const connectionString = `mongodb://${username}:${password}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${dbName}`; // Replace with your actual connection string
    await mongoose.connect(connectionString, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log('Connected to MongoDB');
  } catch (error) {
    console.error('Error connecting to MongoDB:', error);
    process.exit(1); // Exit the process if connection fails
  }
};

module.exports = connectDB;
