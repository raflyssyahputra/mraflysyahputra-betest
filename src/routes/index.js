// routes/userRoutes.js

const express = require('express');
const router = express.Router();
const userController = require('../controllers/UserController');
const authController = require('../controllers/AuthController');
const verifyToken = require('../middleware/middleware');

router.post('/login', authController.login)
router.post('/register', authController.register)
router.post('/user', verifyToken, userController.createUser);
router.get('/user/:id', verifyToken, userController.getUserById);
router.put('/user/:id', verifyToken, userController.updateUser);
router.delete('/user/:id', verifyToken, userController.deleteUser);
router.get('/user', verifyToken, userController.getAllUsers);

module.exports = router;
