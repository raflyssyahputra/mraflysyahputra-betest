// models/User.js

const mongoose = require('mongoose');

const authSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true, unique: true }
});

const Auth = mongoose.model('Auth', authSchema);

module.exports = Auth;
