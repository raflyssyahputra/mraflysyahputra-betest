// services/kafkaConsumer.js

const kafka = require('kafka-node');
const userService = require('./UserService');
require('dotenv').config();

class KafkaConsumer {
  constructor() {
    this.client = new kafka.KafkaClient({ kafkaHost: `${process.env.KAFKA_HOST}:${process.env.KAFKA_PORT}` });
    this.consumer = new kafka.Consumer(
      this.client,
      [{ topic: 'kafka_mraflysyahputra_betest', partition: 0 }],
      { autoCommit: true }
    );

    this.consumer.on('message', async (message) => {
      try {
        const userData = JSON.parse(message.value);
        await userService.consumeUserData(userData);
      } catch (error) {
        console.error('Error consuming message:', error);
      }
    });

    this.consumer.on('error', (err) => {
      console.error('Kafka consumer error:', err);
    });
  }
}

module.exports = new KafkaConsumer();
