// services/kafkaProducer.js

const kafka = require('kafka-node');
require('dotenv').config();

class KafkaProducer {
  constructor() {
    this.client = new kafka.KafkaClient({ kafkaHost: `${process.env.KAFKA_HOST}:${process.env.KAFKA_PORT}` });
    this.producer = new kafka.Producer(this.client);
    this.producer.on('ready', () => {
      console.log('Kafka producer is ready');
    });
    this.producer.on('error', (err) => {
      console.error('Kafka producer error:', err);
    });
  }

  produceUserToKafka(userData) {
    const payload = [{
      topic: 'kafka_mraflysyahputra_betest-insert-user',
      messages: JSON.stringify(userData)
    }];

    this.producer.send(payload, (err, data) => {
      if (err) {
        console.error('Error sending message to Kafka:', err);
      } else {
        console.log('Message sent to Kafka:', data);
      }
    });
  }

  produceAuthToKafka(authData) {
    const payload = [{
      topic: 'kafka_mraflysyahputra_betest-auth-register',
      messages: JSON.stringify(authData)
    }];

    this.producer.send(payload, (err, data) => {
      if (err) {
        console.error('Error sending message to Kafka:', err);
      } else {
        console.log('Message sent to Kafka:', data);
      }
    });
  }

  produceAuthToKafka(user, username) {
    const payload = [{
      topic: 'kafka_mraflysyahputra_betest-login',
      messages: JSON.stringify({username, userId: user._id})
    }];

    this.producer.send(payload, (err, data) => {
      if (err) {
        console.error('Error sending message to Kafka:', err);
      } else {
        console.log('Message sent to Kafka:', data);
      }
    });
  }
}

module.exports = new KafkaProducer();
