// services/userService.js

const userRepository = require('../repositories/UserRepository');
const authRepository = require('../repositories/AuthRepository');
const cacheService = require('./CacheService');
const kafkaProducer = require('./KafkaProducer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();

class AuthService {

  async login(username, password){
    const loginCacheToken = await cacheService.getLoginAuth(username);

    if(loginCacheToken){
      return loginCacheToken;
    }

    // If not in cache, fetch from MongoDB
    const auth = await authRepository.getAuthByEmail(username);
    if (!auth) {
      // return res.status(401).json({ message: 'Invalid username or password' });
      throw {
        status: 401,
        message: 'Invalid username or password'
      }
    }
    // Compare password hashes
    const isMatch = await bcrypt.compare(password, auth.password);
    if (!isMatch) {
      // return res.status(401).json({ message: 'Invalid username or password' });
      throw {
        status: 401,
        message: 'Invalid username or password'
      }
    }

    const token = jwt.sign({ authUsername: auth.username }, process.env.JWT_SECRET);

    await cacheService.setLoginAuth(auth, username);
    kafkaProducer.produceAuthToKafka(auth, username);
    
    return token;
  }

  async createAuth(authData) {  
    const auth = await authRepository.createAuth(authData);
    await cacheService.setAuthDataInCache(auth._id.toString(), auth);
    kafkaProducer.produceAuthToKafka(auth);
    return auth;
  }

}

module.exports = new AuthService();
