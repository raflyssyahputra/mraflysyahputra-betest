const redis = require('redis');
require('dotenv').config();
const jwt = require('jsonwebtoken');

class CacheService {
  constructor() {
    this.client = redis.createClient({
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASSWORD
    });
    this.client.connect();
    // Handle client errors
    this.client.on('error', (err) => {
      console.error('Redis Client Error:', err);
    });

    // Handle client disconnection
    this.client.on('end', () => {
      console.log('Successfully disconnected from Redis server!');
    });
  }

  async getLoginAuth(username){
    const cachedUser = await this.client.get(`redis_mraflysyahputra_ betest-authLogin:${username}`);
    if (cachedUser) {
      const auth = JSON.parse(cachedUser);
      const token = jwt.sign({ authUsername: auth.username }, process.env.JWT_SECRET);
      return token ;
    }
    return null
  }

  async setLoginAuth(user, username) {
    await this.client.set(`redis_mraflysyahputra_ betest-authLogin:${username}`, JSON.stringify(user), 'EX', 7200);
  }

  async getAllUsers() {
    return await this.client.get('redis_mraflysyahputra_ betest-users');
  }

  async setAllUsers(users) {
    this.client.set('redis_mraflysyahputra_ betest-users', JSON.stringify(users), 60, (err, reply) => {
      if (err) console.error('Error setting data in cache:', err);
      else console.log('Successfully set data in cache:', reply);
    });
  }

  async getUserDataFromCache(userId) {
    const cachedUser = await this.client.get(`redis_mraflysyahputra_ betest-user-${userId}`);
    if (cachedUser) {
        return JSON.parse(cachedUser);
    }
    return user;
  }

  async setUserDataInCache(userId, userData) {
    this.client.set(`redis_mraflysyahputra_ betest-user-${userId}`, JSON.stringify(userData), (err, reply) => {
      if (err) console.error('Error setting data in cache:', err);
      else console.log('Successfully set data in cache:', reply);
    });
  }

  async deleteUserFromCache(userId) {
    this.client.del(`redis_mraflysyahputra_ betest-user-${userId}`, (err, reply) => {
      if (err) console.error('Error deleting data from cache:', err);
      else console.log('Successfully deleted data from cache:', reply);
    });
  }

  async setAuthDataInCache(authId, authData) {
    this.client.set(`redis_mraflysyahputra_ betest-auth:${authId}`, JSON.stringify(authData), (err, reply) => {
      if (err) console.error('Error setting data in cache:', err);
      else console.log('Successfully set data in cache:', reply);
    });
  }
}

module.exports = new CacheService();
