// services/userService.js

const userRepository = require('../repositories/UserRepository');
const authRepository = require('../repositories/AuthRepository');
const cacheService = require('./CacheService');
const kafkaProducer = require('./KafkaProducer');
require('dotenv').config();

class UserService {

  async getAllUsers() {
    const cachedUsers = await cacheService.getAllUsers();
    const cacheUserJson = JSON.parse(cachedUsers);
    if (cacheUserJson.length != 0) {
        return cacheUserJson;
    }
    const users = await userRepository.getAllUsers();
    await cacheService.setAllUsers(users);
    return users;
  }

  async createUser(userData) {  
    const user = await userRepository.createUser(userData);
    await cacheService.setUserDataInCache(user._id.toString(), user);
    kafkaProducer.produceUserToKafka(user);
    return user;
  }

  async getUserById(userId) {
    let user = await cacheService.getUserDataFromCache(userId);
    if (!user) {
      user = await userRepository.getUserById(userId);
      if (user) {
        await cacheService.setUserDataInCache(userId, user);
      }
    }
    return user;
  }

  async updateUser(userId, userData) {
    await userRepository.updateUser(userId, userData);
    const user = await userRepository.getUserById(userId);
    await cacheService.setUserDataInCache(userId, user);
    kafkaProducer.produceUserToKafka(user);
    return user;
  }

  async deleteUser(userId) {
    await userRepository.deleteUser(userId);
    await cacheService.deleteUserFromCache(userId);
  }

  async consumeUserData(userData) {
    try {
      // Insert the received user data into MongoDB
      const user = await userRepository.createUser(userData);
      console.log('User data consumed from Kafka and inserted into MongoDB:', user);
    } catch (error) {
      console.error('Error consuming user data:', error);
    }
  }
}

module.exports = new UserService();
