// controllers/userController.js

const authService = require('../services/AuthService');
const authRepository = require('../repositories/AuthRepository');
const bcrypt = require('bcrypt');
const saltRounds = 10;

class AuthController {

  async login(req, res){
    const { username, password } = req.body;

    if(username == '' || password == ''){
      return res.status(400).json({ 
        success: false,
        msg: `username or password can't be empty`
      });
    }
    try {
      const token = await authService.login(username, password);
      return res.json({ 
        success: true,
        token: token
       });
    } catch (err) {
      console.error('Error during login:', err);
      res.status(err.status).json({ message: err.message });
    }
  }

  async register(req, res) {
    try {
      const { username, password } = req.body;

      if (!username || !password) {
        return res.status(400).json({ message: 'Username and password are required' });
      }
      const authCheck = await authRepository.getUniqueAuth(req.body);
      if(authCheck.length != 0){
        return res.status(400).json({ 
          success: false,
          msg: 'User already exists. Please check your data.'
        });
      }
      const body = {
        username: username,
        password: await bcrypt.hash(password, saltRounds)
      }
      const auth = await authService.createAuth(body);
      return res.status(201).json({
        success: true,
        data: auth
      });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

}

module.exports = new AuthController();
