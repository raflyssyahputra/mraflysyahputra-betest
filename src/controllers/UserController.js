// controllers/userController.js

const userService = require('../services/UserService');
const userRepository = require('../repositories/UserRepository');

class UserController {

  async login(req, res){
    const { username, password } = req.body;

    if(username == '' || password == ''){
      res.status(400).json({ 
        success: false,
        msg: `username or password can't be empty`
      });
    }
    try {
      const token = await userService.login(username, password);
      res.json({ 
        success: true,
        data: token
       });
    } catch (err) {
      console.error('Error during login:', err);
      res.status(err.status).json({ message: err.message });
    }
  }

  async getAllUsers(req, res) {
    try {
      const user = await userService.getAllUsers();
      return res.status(201).json({
        success: true,
        data: user
      });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async createUser(req, res) {
    try {
      const userCheck = await userRepository.getUniqueUser(req.body);
      if(userCheck.length != 0){
        res.status(400).json({ 
          success: false,
          msg: 'User already exists. Please check your data.'
        });
      }else{
        const user = await userService.createUser(req.body);
        res.status(201).json({
          success: true,
          data: user
        });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async getUserById(req, res) {
    try {
      const user = await userService.getUserById(req.params.id);
      if (!user) {
        res.status(404).json({ error: 'User not found' });
      } else {
        res.json({
          success: true,
          data: user
        });
      }
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async updateUser(req, res) {
    try {
      const user = await userService.updateUser(req.params.id, req.body);
      res.json({
        success: true,
        data: user
      });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  async deleteUser(req, res) {
    try {
      await userService.deleteUser(req.params.id);
      res.sendStatus(204);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
}

module.exports = new UserController();
