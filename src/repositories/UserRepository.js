// repositories/userRepository.js

const User = require('../models/User');

class UserRepository {

  async getAllUsers() {
    return await User.find();
  }

  async getUniqueUser(userData) {
    const filter = {
      $or: [
        { userName: userData.userName },
        { accountNumber: userData.accountNumber },
        { emailAddress: userData.emailAddress },
        { identityNumber: userData.identityNumber },
      ],
    };
    return await User.find(filter);
  }

  async getUserByEmail(username) {
    const filter = {
      $or: [
        { userName: username },
      ],
    };
    const user = await User.find(filter)
    return user[0];
  }

  async createUser(userData) {
    return await User.create(userData);
  }

  async getUserById(userId) {
    return await User.findById(userId);
  }

  async updateUser(userId, userData) {
    return await User.findByIdAndUpdate(userId, userData, { new: true });
  }

  async deleteUser(userId) {
    await User.findByIdAndDelete(userId);
  }
}

module.exports = new UserRepository();
