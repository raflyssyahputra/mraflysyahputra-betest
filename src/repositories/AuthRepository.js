// repositories/userRepository.js

const Auth = require('../models/Auth');

class AuthRepository {

  async getUniqueAuth(authData) {
    const filter = {
      $or: [
        { username: authData.username },
      ],
    };
    return await Auth.find(filter);
  }

  async getAuthByEmail(username) {
    const filter = {
      $or: [
        { username: username },
      ],
    };
    const auth = await Auth.find(filter)
    return auth[0];
  }

  async createAuth(authData) {
    return await Auth.create(authData);
  }

}

module.exports = new AuthRepository();
