FROM mongo:latest

# Set environment variables for username, password, and database name
ENV MONGO_INITDB_ROOT_USERNAME=your_username
ENV MONGO_INITDB_ROOT_PASSWORD=test123
ENV MONGO_INITDB_DATABASE=your_database

# Create a directory for initialization scripts
RUN mkdir -p /docker-entrypoint-initdb.d

# Copy initialization script
COPY mongo-init.js /docker-entrypoint-initdb.d/mongo-init.js

# Expose the default MongoDB port
EXPOSE 27017

# Command to run MongoDB with initialization scripts
CMD ["mongod", "--quiet", "--oplogSize", "128"]