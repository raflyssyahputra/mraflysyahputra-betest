FROM node:18-alpine

RUN mkdir -p /home/node/node_modules && chown -R node:node /home/node/

WORKDIR /home/node/

COPY package*.json ./

USER node

COPY --chown=node:node . .

RUN npm install

EXPOSE 3000

CMD [ "node", "src/index.js" ]
