
// // Connect to the admin database
// db = connect("mongodb://localhost:27017/admin");

// // Ensure the admin user is authenticated (optional, for enhanced security)
// db.auth({
//   username: "root",
//   password: "test123"
// });

// // Create a dedicated user with specific database access
// // db.adminCommand({
// //   createUser: "mraflysyahputra",
// //   pwd: "test123",
// //   roles: [
// //     { role: "dbOwner", db: "db_mraflysyahputra_betest" }, // Grant access to specific database
// //   ],
// // });

// db.createUser({
//   user: "mraflysyahputra",
//   pwd: "test123",
//   roles: [{ role: "dbOwner", db: "db_mraflysyahputra_betest" }]
// })

// // db.dropUser("mraflysyahputra")



// // Optionally, create additional users with different roles and permissions
// // db.adminCommand({
// //   createUser: "your_other_username",
// //   pwd: "your_other_password",
// //   roles: [
// //     { role: "readWrite", db: "your_database_name" }, // Read-write access
// //   ],
// // });

// // Optionally, create collections and insert initial data (if needed)
// // db = db.getSiblingDB("your_database_name");
// // db.createCollection("your_collection_name");
// // db.your_collection_name.insert([{ /* your data objects */ }]);

// // Close the connection to the admin database
// db.close();

// console.log("MongoDB initialization script completed successfully.");

db = getSiblingDB('db_mraflysyahputra_betest');

// Replace with your desired username and password
db.createUser({
  user: "mraflysyahputra",
  pwd: "test123",
  roles: [ { role: "dbOwner", db: "db_mraflysyahputra_betest" } ]
});
